package com.home.android.testapp.network;

import com.home.android.testapp.model.Response;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by dmytroubogiy on 27.04.18.
 */

public interface ApiService {

    @GET("/s/fk3d5kg6cptkpr6/menu.json?dl=1")
    Observable<Response> getMenuItems();
}
