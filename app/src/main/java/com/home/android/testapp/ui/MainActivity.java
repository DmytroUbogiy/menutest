package com.home.android.testapp.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.home.android.testapp.model.CustomMenuItem;

import com.home.android.testapp.R;
import com.home.android.testapp.model.Response;
import com.home.android.testapp.model.ResponseMenu;
import com.home.android.testapp.ui.image.ImageFragment;
import com.home.android.testapp.ui.text.TextFragment;
import com.home.android.testapp.ui.url.UrlFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MainView {


    private NavigationView navigationView;

    private MainPresenter mainPresenter;

    private Unbinder unbinder;

    private int position;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        mainPresenter = new MainPresenterImpl();
        mainPresenter.attach(this);
        if (savedInstanceState != null) {
            position = (int) savedInstanceState.get("position");
            List<ResponseMenu> customMenuItems = savedInstanceState.getParcelableArrayList("items");
            mainPresenter.setCustomMenuList(customMenuItems);
        }
        mainPresenter.loadMenuItems();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("position", position);
        List<ResponseMenu> parcebleList = new ArrayList<>(mainPresenter.getCustomMenuList().size());
        for (int i = 0; i < mainPresenter.getCustomMenuList().size(); ++i) {
            parcebleList.add(mainPresenter.getCustomMenuList().get(i).menu);
        }
        outState.putParcelableArrayList("items", (ArrayList<ResponseMenu>) parcebleList);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        position = item.getItemId();
        mainPresenter.onMenuItemClicked(position);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMenuItemsLoaded(List<CustomMenuItem> customMenuItemList) {
        navigationView.setNavigationItemSelectedListener(this);
        Menu menu = navigationView.getMenu();
        for (int i = 0; i < customMenuItemList.size(); ++i) {
            CustomMenuItem customMenuItem = customMenuItemList.get(i);
            MenuItem menuItem = menu.add(i, i, i, customMenuItem.menu.name);
            menuItem.setCheckable(true);
        }
        menu.getItem(position).setChecked(true);
        mainPresenter.onMenuItemClicked(position);
    }

    @Override
    public void openTextType(String param) {
        TextFragment textFragment = TextFragment.newInstance(param);
        openFragment(textFragment);
    }

    @Override
    public void openImageType(String param) {
        ImageFragment imageFragment = ImageFragment.newInstance(param);
        openFragment(imageFragment);
    }

    @Override
    public void openUrlType(String param) {
        UrlFragment urlFragment = UrlFragment.newInstance(param);
        openFragment(urlFragment);
    }

    @Override
    public void displayError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    private void openFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        if (mainPresenter != null) {
            mainPresenter.onDestroy();
        }
        super.onDestroy();
    }
}
