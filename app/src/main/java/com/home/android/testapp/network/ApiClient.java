package com.home.android.testapp.network;

import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dmytroubogiy on 27.04.18.
 */

public class ApiClient {

    private static final int CONNECT_TIMEOUT_IN_MS = 30000;
    private static final String BASE_URL = "https://www.dropbox.com";

    private static ApiClient instance;
    private ApiService apiService;

    public static ApiClient getInstance() {
        if (instance == null) {
            instance = new ApiClient();
        }
        return instance;
    }

    private ApiClient() {
        OkHttpClient okHttpClient = createOkHttpClient();
        Retrofit retrofit = createRetrofit(okHttpClient);
        apiService = createApiService(retrofit);
    }

    private OkHttpClient createOkHttpClient() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT_IN_MS, TimeUnit.MILLISECONDS)
                .addInterceptor(loggingInterceptor)
                .build();
    }

    private Retrofit createRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    private ApiService createApiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    public ApiService getApiService() {
        return apiService;
    }
}
