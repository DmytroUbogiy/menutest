package com.home.android.testapp.ui;

import com.home.android.testapp.model.CustomMenuItem;
import com.home.android.testapp.model.Response;

import java.util.List;

/**
 * Created by dmytroubogiy on 27.04.18.
 */

public interface MainView {

    void onMenuItemsLoaded(List<CustomMenuItem> customMenuItemList);
    void openTextType(String param);
    void openImageType(String param);
    void openUrlType(String param);
    void displayError(String message);
    void showLoading();
    void hideLoading();
}
