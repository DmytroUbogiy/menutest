package com.home.android.testapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by dmytroubogiy on 27.04.18.
 */

public class ResponseMenu implements Parcelable {

    public String name;
    public String function;
    public String param;

    public ResponseMenu(Parcel in) {
        name = in.readString();
        function = in.readString();
        param = in.readString();
    }

    public static final Creator<ResponseMenu> CREATOR = new Creator<ResponseMenu>() {
        @Override
        public ResponseMenu createFromParcel(Parcel in) {
            return new ResponseMenu(in);
        }

        @Override
        public ResponseMenu[] newArray(int size) {
            return new ResponseMenu[size];
        }
    };



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(function);
        dest.writeString(param);
    }
}
