package com.home.android.testapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by dmytroubogiy on 27.04.18.
 */

public class CustomMenuItem implements Parcelable {

    public ResponseMenu menu;
    public MenuType menuType;

    public CustomMenuItem() {

    }

    public CustomMenuItem(Parcel in) {
        menu = in.readParcelable(ResponseMenu.class.getClassLoader());
        menuType = (MenuType.valueOf(in.readString()));
    }

    public static final Creator<CustomMenuItem> CREATOR = new Creator<CustomMenuItem>() {
        @Override
        public CustomMenuItem createFromParcel(Parcel in) {
            return new CustomMenuItem(in);
        }

        @Override
        public CustomMenuItem[] newArray(int size) {
            return new CustomMenuItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(menu, flags);
        dest.writeString(menuType.getType());
    }
}
