package com.home.android.testapp.ui;

import com.home.android.testapp.model.CustomMenuItem;
import com.home.android.testapp.model.MenuType;
import com.home.android.testapp.model.Response;
import com.home.android.testapp.model.ResponseMenu;
import com.home.android.testapp.network.ApiClient;
import com.home.android.testapp.network.ApiService;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by dmytroubogiy on 27.04.18.
 */

public class MainPresenterImpl implements MainPresenter {

    private MainView mainView;
    private List<CustomMenuItem> customMenuItemList;
    private CompositeDisposable compositeDisposable;

    @Override
    public void attach(MainView mainView) {
        this.mainView = mainView;
    }

    @Override
    public void loadMenuItems() {
        if (customMenuItemList == null || customMenuItemList.size() == 0) {
            mainView.showLoading();
            ApiService apiService = ApiClient.getInstance().getApiService();
            compositeDisposable = new CompositeDisposable();
            compositeDisposable.add(apiService.getMenuItems()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .flatMap(new Function<Response, ObservableSource<List<CustomMenuItem>>>() {
                        @Override
                        public ObservableSource<List<CustomMenuItem>> apply(Response response) throws Exception {
                            List<CustomMenuItem> customMenuItems = formMenuItems(response.menu);
                            return Observable.just(customMenuItems);
                        }
                    })
                    .subscribe(new Consumer<List<CustomMenuItem>>() {
                        @Override
                        public void accept(List<CustomMenuItem> menuItemList) throws Exception {
                            if (!isViewAttached()) {
                                return;
                            }
                            mainView.hideLoading();
                            customMenuItemList = menuItemList;
                            mainView.onMenuItemsLoaded(customMenuItemList);
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            if (!isViewAttached()) {
                                return;
                            }
                            mainView.hideLoading();
                            mainView.displayError(throwable.getMessage());
                        }
                    })
            );
        }
        else {
            mainView.onMenuItemsLoaded(customMenuItemList);
        }
    }

    @Override
    public void onMenuItemClicked(int position) {
        CustomMenuItem customMenuItem = customMenuItemList.get(position);
        MenuType menuType = customMenuItem.menuType;
        switch (menuType) {
            case URL:
                mainView.openUrlType(customMenuItem.menu.param);
                break;
            case TEXT:
                mainView.openTextType(customMenuItem.menu.param);
                break;
            case IMAGE:
                mainView.openImageType(customMenuItem.menu.param);
                break;
        }
    }

    @Override
    public List<CustomMenuItem> getCustomMenuList() {
        return customMenuItemList;
    }

    @Override
    public void setCustomMenuList(List<ResponseMenu> customMenuList) {
        this.customMenuItemList = formMenuItems(customMenuList);
    }

    @Override
    public void onDestroy() {
        mainView = null;
        if (compositeDisposable != null) {
            compositeDisposable.clear();
            compositeDisposable = null;
        }
        if (customMenuItemList != null) {
            customMenuItemList.clear();
            customMenuItemList = null;
        }
    }

    private boolean isViewAttached() {
        return mainView != null;
    }

    private List<CustomMenuItem> formMenuItems(List<ResponseMenu> responseMenus) {
        List<CustomMenuItem> customMenuItemList = new ArrayList<>(responseMenus.size());
        for (int i = 0; i < responseMenus.size(); ++i) {
            CustomMenuItem customMenuItem = new CustomMenuItem();
            ResponseMenu responseMenu = responseMenus.get(i);
            customMenuItem.menu = responseMenu;
            customMenuItem.menuType = setMenuType(responseMenu.function);
            customMenuItemList.add(customMenuItem);
        }
        return customMenuItemList;
    }

    private MenuType setMenuType(String function) {
        if (function.equals(MenuType.IMAGE.getType())) {
            return MenuType.IMAGE;
        }
        else if (function.equals(MenuType.TEXT.getType())) {
            return MenuType.TEXT;
        }
        else {
            return MenuType.URL;
        }
    }
}
