package com.home.android.testapp.model;

/**
 * Created by dmytroubogiy on 27.04.18.
 */

public enum MenuType {

    TEXT("text"), IMAGE("image"), URL("url");

    private String type;

    MenuType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
