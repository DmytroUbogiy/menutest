package com.home.android.testapp.ui;

import com.home.android.testapp.model.CustomMenuItem;
import com.home.android.testapp.model.ResponseMenu;

import java.util.List;

/**
 * Created by dmytroubogiy on 27.04.18.
 */

public interface MainPresenter {

    void attach(MainView mainView);
    void loadMenuItems();
    void onMenuItemClicked(int position);
    List<CustomMenuItem> getCustomMenuList();
    void setCustomMenuList(List<ResponseMenu> customMenuList);
    void onDestroy();
}
